# Git常用命令

1. 克隆（Clone）： 从远程仓库克隆一个项目到本地。

        git clone <repository_url>

2. 添加（Add）： 将文件添加到暂存区，准备进行提交。

       git add <file_name>

3. 提交（Commit）： 将暂存区的更改提交到本地仓库，并添加提交信息。

       git commit -m "Commit message"

4. 推送（Push）： 将本地的提交推送到远程仓库。

       git push origin <branch_name>
5. 拉取（Pull）： 从远程仓库拉取最新代码到本地。

       git pull origin <branch_name>

6. 分支（Branch）： 创建、切换和查看分支。

* 创建分支：

       git branch <new_branch_name>
* 切换分支：

       git checkout <branch_name>
* 创建并切换分支（Git 2.23+）：

       git switch -c <new_branch_name>
* 查看分支：

       git branch

7. 合并（Merge）： 将一个分支的更改合并到另一个分支。

       git merge <branch_name>

8. 拉取请求（Pull Request）： 

   在远程仓库中创建一个拉取请求，请求将您的更改合并到另一个分支。
9. 状态（Status）： 查看工作区和暂存区的状态。

       git status
10. 日志（Log）： 查看提交历史记录。

        git log

11. 远程仓库（Remote）： 管理远程仓库的连接。
* 添加远程仓库：

       git remote add <remote_name> <repository_url>

* 查看远程仓库：

       git remote add <remote_name> <repository_url>

12.  撤销（Revert）： 撤销之前的提交。

          git revert <commit_hash>

13.  重置（Reset）： 回退到指定提交，可以清空暂存区或工作区。

* 软重置，保留更改：

      git reset --soft <commit_hash>

* 混合重置，清空暂存区：

      git reset --mixed <commit_hash>

* 硬重置，清空暂存区和工作区（谨慎使用）：

      git reset --hard <commit_hash>

14. 标签（Tag）： 创建和管理版本标签。
* 创建标签：

      git tag <tag_name> <commit_hash>

* 查看标签：

      git tag

15. 用户名设置

    - 全局用户名设置

      - 设置用户名

        ```shell
        git config --global user.name "您的用户名"
        ```

      - 确认正确设置了 Git 用户名：

        ```shell
        git config --global user.name
        ```

    - 项目用户名设置

      - 将当前工作目录更改为您想要在其中配置与 Git 提交关联的名称的本地仓库。

      - 设置 Git 用户名：

        ```shell
        git config user.name "您的用户名"
        ```

      - 确认正确设置了 Git 用户名：

        ```shell
        git config user.name
        ```

# 欢迎补充和修改

更多请参考[GitHub 文档](https://docs.github.com/zh)

---







