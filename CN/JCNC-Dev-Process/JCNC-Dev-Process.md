# 1. 开发流程
基于**AoneFlow**开发流程，具体请阅读：[在阿里，我们如何管理代码分支？](https://developer.aliyun.com/article/573549)
## 1.1 IntelliJ IDEA版
###  步骤一 Fork JCNC/JNotepad或同步JCNC/JNotepad到个人仓库
#### 首次开发，进行Fork操作
![输入图片说明](img/1.png "屏幕截图")
#### 非首次开发，进行同步操作
![输入图片说明](img/2.png "屏幕截图")

### 步骤二 clone个人仓库或fetch
#### 首次开发，clone个人仓库
![输入图片说明](img/3.png "屏幕截图")
#### 非首次开发，执行fetch
![输入图片说明](img/4.png "屏幕截图")

### 步骤三 从remote下的master分支创建本地开发分支
![输入图片说明](img/5.png "屏幕截图")
![输入图片说明](img/6.png "屏幕截图")

分支命名规则见：1.3节

### 步骤四 推送本地分支到远程

### 步骤五 发起Pull Request（PR）
![输入图片说明](img/7.png "屏幕截图")

**注意：目标分支选择预期要发布的release分支**

## 1.2 GIT命令行版本
步骤一、步骤二、步骤五同1.1操作
```shell
# 步骤三
git fetch
git checkout origin/master && git checkout -b feature-demo
# 步骤四
git push origin feature-demo
```

## 1.3 分支命名规则
|issue类别|分支名格式|示例|
|--------|--------|----|
|功能/优化/文档修改|feature-issue编号|feature-I7W9LX|
|bug fix| fix-issue编号| fix-I7W9LX|
|代码重构|refactor-issue编号|refactor-I7W9LX|

# 2. IDEA插件配置
* 安装Resource Bundle插件
  ![输入图片说明]( img/8.png "屏幕截图")
* 安装成功后，打开i18n.properties，可以看到Resource Bundle tab
  ![输入图片说明]( img/9.png "屏幕截图")