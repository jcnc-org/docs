# JNotepad 开发者指南
欢迎使用 JNotepad 开发者指南。借助这些文档，您可以了解如何使用、开发和打包[JNotepad](https://gitee.com/jcnc-org/JNotepad) 。

## 新手指南
如果您是首次接触 JavaFX，并希望开始编写代码，请从[构建首个 JavaFX 应用](../../Build-The-First-JavaFX-Application/Build-The-First-JavaFX-Application.md)教程开始。

此外，请通过查看下面的其他资源或社区来学习 JNotepad 开发：

- [JavaFX 官网](https://openjfx.io/)
- [JavaFX API](https://openjfx.io/javadoc/21/)
- [QQ 交流群](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=x3QF-jrJAKTiwu8kV5-giBk2ow66Kzyr&authKey=qNqrQauD7Ra4fXH%2Ftu4ylHXCyrf2EOYj9oMYOmFjlzYmrgDL8Yd0m2qhrQQEBL25&noverify=0&group_code=386279455)

## 基础知识

- [JavaFX 基础知识](../../JavaFX-Knowledge/JavaFX-Knowledge.md)

## JNotepad 文档

- [JNotepad 文档](https://gitee.com/jcnc-org/JNotepad)