# 构建首个JavaFX应用
构建首个JavaFX应用的文档编写可以分为以下几个步骤。这个示例将创建一个简单的JavaFX应用程序，展示一个包含 "Hello, JNotepad!" 文本的窗口。

示例JavaFX应用开发环境:

| 环境配置 |          版本号           |
|:----:|:----------------------:|
| 操作系统 |       Windows 11       |
| IDE  | IntelliJ IDEA 2023.2.1 |
| JDK  |     OpenJDK 17.0.2     |

# 步骤1：准备开发环境

在开始编写JavaFX应用之前，确保您已经完成以下准备工作：

1. 安装Java Development Kit (JDK)：确保您的计算机上安装了Java SE Development Kit（JDK）。

您可以从Oracle官网或者使用OpenJDK获取。

> 推荐使用**OpenJDK 17**及以上的**GA Releases**。
> 
> - [点击下载 OpenJDK 17](https://jdk.java.net/java-se-ri/17)
> - [点击下载 OpenJDK 21](https://jdk.java.net/java-se-ri/21)

2. 安装集成开发环境（IDE）：您可以选择使用Eclipse、**IntelliJ IDEA**、NetBeans等集成开发环境来开发JavaFX应用程序。确保IDE已经安装并配置好。

> 推荐使用**IntelliJ IDEA**,并且以**IntelliJ IDEA**作为集成开发环境。
> 
> - [点击下载 IntelliJ IDEA](https://www.jetbrains.com/zh-cn/idea/)

# 步骤2：创建JavaFX项目

在您的IDE中，创建一个新的JavaFX项目：

1. 打开IDE并选择创建一个新的JavaFX项目。
   ![img.png](../Dev-Guide/JNotepad-Dev-Guide/img/img.png)
2. 配置项目名称和存储位置。
   ![img_1.png](../Dev-Guide/JNotepad-Dev-Guide/img/img_1.png)
3. 点击下一步。
   ![img_2.png](../Dev-Guide/JNotepad-Dev-Guide/img/img_2.png)
4. 点击创建
   ![img_3.png](../Dev-Guide/JNotepad-Dev-Guide/img/img_3.png)
5. 删除初始化代码
   ![img_4.png](../Dev-Guide/JNotepad-Dev-Guide/img/img_4.png)
6. 新建项目完毕
   ![img_7.png](../Dev-Guide/JNotepad-Dev-Guide/img/img_7.png)

# 步骤3：编写JavaFX应用程序代码

现在，您可以开始编写JavaFX应用程序的代码。以下是一个简单的示例：

我们把这些代码复制进项目:

MyJavaFXApp.java:

```java
package com.example.demo;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * @author luke
 */
public class MyJavaFXApp extends Application {

    @Override
    public void start(Stage primaryStage) {
        // 创建一个Label来显示文本
        Label label = new Label("Hello, JNotepad!");

        // 创建一个StackPane布局，将Label置于其中
        StackPane root = new StackPane();
        root.getChildren().add(label);

        // 创建一个Scene，并将StackPane设置为根节点
        Scene scene = new Scene(root, 300, 200);

        // 设置主舞台的标题和Scene
        primaryStage.setTitle("My JavaFX App");
        primaryStage.setScene(scene);

        // 显示主舞台
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
```

module-info.java:

```java
module com.example.demo {
    requires javafx.controls;
    
        opens com.example.demo;
    }
```

pom.xml:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.example</groupId>
    <artifactId>Demo</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>Demo</name>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <junit.version>5.9.2</junit.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-controls</artifactId>
            <version>17.0.6</version>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-fxml</artifactId>
            <version>17.0.6</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.11.0</version>
                <configuration>
                    <source>17</source>
                    <target>17</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-maven-plugin</artifactId>
                <version>0.0.8</version>
                <executions>
                    <execution>
                        <!-- Default configuration for running with: mvn clean javafx:run -->
                        <id>default-cli</id>
                        <configuration>
                            <mainClass>com.example.demo/com.example.demo.MyJavaFXApp</mainClass>
                            <launcher>app</launcher>
                            <jlinkZipName>app</jlinkZipName>
                            <jlinkImageName>app</jlinkImageName>
                            <noManPages>true</noManPages>
                            <stripDebug>true</stripDebug>
                            <noHeaderFiles>true</noHeaderFiles>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```

# 步骤4：运行应用程序

在IDE中，右键文本编辑区,选择运行（Run）或者调试（Debug）您的JavaFX应用程序。这将启动JavaFX应用程序，并显示一个窗口，其中包含 "Hello, JavaFX!" 文本。
![img_6.png](../Dev-Guide/JNotepad-Dev-Guide/img/img_6.png)
![img_8.png](../Dev-Guide/JNotepad-Dev-Guide/img/img_8.png)
# 步骤5：构建和打包

目前为止您的JavaFX应用程序编写完成，您可以构建它并打包到目标平台分发。

 [构建和打包(构建中...)](../构建和打包/构建和打包.md)

# 总结
完成以上步骤后，您就成功构建了一个简单的JavaFX应用程序。您可以根据项目的需求进行扩展和改进，添加更多的UI元素和功能。希望这份文档可以帮助您入门JavaFX应用程序的开发。

