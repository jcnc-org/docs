# JCNC开发者指南

本目录是JCNC开发者指南。目录内容包括：

## 新手指南

[第一次接触开源项目?](../First-Exposure-To-Open-Source-Projects/First-Exposure-To-Open-Source-Projects.md)

[Git常用命令](../Git-Common-Commands/Git-Common-Commands.md)

## 项目文档

[JNotepad开发者指南](../Dev-Guide/JNotepad-Dev-Guide/JNotepad-Dev-Guide.md)

[IDEology开发者指南](../Dev-Guide/IDEology-Doc/IDEology-Dev-Guide.md)

[JCNC项目开发流程](../JCNC-Dev-Process/JCNC-Dev-Process.md)



## 欢迎补充和修复文档!
